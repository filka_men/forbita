package entity;

import constants.PathConstants;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;

public class Atm {

    public static Card checkIdCard(String idCard, ArrayList<Card> cardUsers) {
        for (Card card : cardUsers) {
            if (card.getIdCard().equals(idCard)) {
                return card;
            }
        }
        return null;
    }

    public static Boolean checkPinCodeCard(int pinCode, Card card) {
        if (card.getPinCode() == pinCode && card.getActivity()) {
            return true;
        }
        return false;
    }

    private static void safePaymentCard(ArrayList<Card> cardUsers) throws IOException {
        FileWriter writer = new FileWriter(PathConstants.PATH_DATABASE_USER);
        for (int i = 0; i < cardUsers.size(); i++) {
            String str = cardUsers.get(i).toString();
            System.out.println(str);
            writer.write(str);
            if (i < cardUsers.size() - 1)
                writer.write("\n");
        }
        writer.close();
    }

    public static void getMoney(Card card, BigDecimal summ, ArrayList<Card> cardUsers) throws IOException {
        for (int i = 0; i < cardUsers.size(); i++) {
            if (cardUsers.get(i).equals(card)) {
                cardUsers.get(i).setBalance(card.getBalance().subtract(summ));
            }
        }
        safePaymentCard(cardUsers);
    }

    public static void polochenieDeneg(Card card, BigDecimal chislo, ArrayList<Card> cardUsers) throws IOException {
        for (int i = 0; i < cardUsers.size(); i++) {
            if (cardUsers.get(i).equals(card)) {
                cardUsers.get(i).setBalance(card.getBalance().add(chislo));
            }
        }
        safePaymentCard(cardUsers);
    }

    public static void isActiviti(Card card, ArrayList<Card> cardUsers) throws IOException {
        for (int i = 0; i < cardUsers.size(); i++) {
            cardUsers.get(i).setActivity(false);
        }
        safePaymentCard(cardUsers);
    }

}
