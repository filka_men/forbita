package view;

import entity.Card;
import parser.CustomFileReader;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.System.exit;

public class Menu {

    private int option = 1;

    private void printMenu(String[] options) {
        for (String option : options) {
            System.out.println(option);
        }
        System.out.print("Choose your option : ");
    }

    private String[] options = {"1- Check the card balance  ",
            "2- deposit money",
            "3- withdraw money",
            "4- Exit",
    };

    public void start() throws FileNotFoundException {
        Card card;
        ArrayList<Card> cards = CustomFileReader.paymentsCards();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the your payment card: ");
        String idCard = scanner.next();
        card = Card.checkIdCard(idCard, cards);
        while (card == null) {
            System.out.println("Please enter payment card ");
            idCard = scanner.next();
            card = Card.checkIdCard(idCard, cards);
        }
        System.out.print("Enter the your pin code: ");
        int pinCode = scanner.nextInt();
        int countAttempt;
        countAttempt = 0;
        while (!Card.checkPinCodeCard(pinCode, card)) {
            System.out.println("Please enter pin code for card ");
            countAttempt++;
            if(countAttempt == 3)
            {
                option = 5;
            } else{
                pinCode = scanner.nextInt();
            }
        }
        while (option != 5) {
            printMenu(options);
            try {
                option = scanner.nextInt();
                switch (option) {
                    case 1:
                        System.out.println(card.getBalance());
                        break;
                    case 3:
                        System.out.println("Please enter the amount you want to withdraw: ");
                        BigDecimal summa = scanner.nextBigDecimal();
                        Card.snitieDeneg(card, summa, cards);
                        break;
                    case 2:
                        System.out.println("Please enter сthe amount you want to deposit: ");
                        summa = scanner.nextBigDecimal();
                        Card.polochenieDeneg(card, summa, cards);
                        break;
                    case 4:
                        exit(0);
                }
            } catch (Exception ex) {
                System.out.println("Please enter an integer value between 1 and " + options.length);
                scanner.next();
            }
        }
    }

}
